#! /bin/bash
IMAGE=centos7icewmvnc:latest
docker stop $(docker container ls -a | grep -i $IMAGE | awk '{print $1}')
docker run -d -p 6080:6080 -p 2022:22 -e GEOMETRY='1920x1080' $IMAGE
#docker run -it -p 6080:6080 -p 2022:22 -e GEOMETRY='1320x720' $IMAGE
