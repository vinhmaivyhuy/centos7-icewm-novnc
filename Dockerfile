#
# Centos Desktop (Gnome) Dockerfile
#
# docker build -t centos7icewmvnc .
# docker run -it -p 5901:5901 -p 22:22 centos7icewmvnc 
# docker run -d -p 5901:5901 -p 22:22 centos7icewmvnc
#
# ssh Tuneling:
# ssh localhost -L 5901:localhost:5901
#

# Pull base image.
FROM centos:7

# Setup enviroment variables

# Update the package manager and upgrade the system
RUN yum -y install epel-release
RUN yum -y update
RUN yum -y install net-tools bzip2 sudo wget which vim 
RUN yum -y install openssh-server openssh-clients
RUN ssh-keygen -A

# Set the locale
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# Install GNOME and tightvnc server.
RUN yum -y install tigervnc-server 
RUN yum -y install xorg xterm xfonts-base xorg-x11-fonts* xauth xinit
RUN yum -y install icewm
RUN yum clean all
RUN /bin/dbus-uuidgen > /etc/machine-id


# Set up VNC
ADD vnc.conf /etc/vnc.conf

# Set up User
RUN useradd -s /bin/bash nguyenv
RUN passwd -d nguyenv
RUN echo "nguyenv  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/nguyenv

ADD ./.icewm/ /home/nguyenv/.icewm/

RUN mkdir /home/nguyenv/.ssh
ADD authorized_keys /home/nguyenv/.ssh/authorized_keys
RUN chmod go-rwx /home/nguyenv/.ssh/authorized_keys

ADD .Xauthority /home/nguyenv/.Xauthority
RUN chmod go-rwx /home/nguyenv/.Xauthority

RUN mkdir -p /home/nguyenv/.vnc
ADD xstartup /home/nguyenv/.vnc/xstartup
ADD config /home/nguyenv/.vnc/config
RUN chmod 755 /home/nguyenv/.vnc/xstartup

ADD passwd /home/nguyenv/.vnc/passwd
RUN chmod go-rwx /home/nguyenv/.vnc/passwd

RUN chown -R nguyenv:nguyenv /home/nguyenv

ADD passwd /root/.vnc/passwd
RUN chmod go-rwx /root/.vnc/passwd

ADD run.sh /root/run.sh
RUN chmod +x /root/*.sh

# Expose ports.
EXPOSE 5901
EXPOSE 22


# Define default command
# CMD ["/root/run.sh"]
#
ENTRYPOINT ["/root/run.sh"]
CMD ["--wait"]

