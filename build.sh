#!/bin/bash
cd ~/sources/gnomevnc

docker container prune --force
docker image prune --force --filter='label=centos7icewmvnc'

docker build --tag centos7icewmvnc:latest --label centos7icewmvnc .


docker image ls
docker ps